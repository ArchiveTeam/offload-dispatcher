package main

import (
	"github.com/inhies/go-bytesize"
	"gopkg.in/yaml.v3"
	"os"
)

type Config struct {
	Listen   string       `yaml:"listen"`
	PromAddr string       `yaml:"prometheus_address"`
	Targets  []ServerSpec `yaml:"targets"`
}

type ServerSpec struct {
	Id               string            `yaml:"id"`
	Url              string            `yaml:"url"`
	MinimumFreeSpace bytesize.ByteSize `yaml:"free_space_min"`
	JobName          string            `yaml:"prom_job_name"`
}

func NewConfigFromFile(fn string) (Config, error) {
	var c Config
	yamlFile, err := os.ReadFile(fn)
	if err != nil {
		return c, err
	}
	err = yaml.Unmarshal(yamlFile, &c)
	if err != nil {
		return c, err
	}

	log.Infof("Loaded config: %v", c)

	return c, nil
}
