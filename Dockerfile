FROM golang:1.21 AS build-stage

WORKDIR /app

COPY go.mod go.sum ./
RUN go mod download

COPY *.go cmd ./

RUN CGO_ENABLED=0 GOOS=linux go build -o /dispatcher ./...

# Run the tests in the container
FROM build-stage AS run-test-stage
RUN go test -v ./...

# Deploy the application binary into a lean image
FROM gcr.io/distroless/base-debian12 AS build-release-stage

WORKDIR /

COPY --from=build-stage /dispatcher /dispatcher

USER nonroot:nonroot
ENV GIN_MODE=release

ENTRYPOINT ["/dispatcher"]